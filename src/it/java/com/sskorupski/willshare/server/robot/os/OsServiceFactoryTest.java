package com.sskorupski.willshare.server.robot.os;

import com.sskorupski.willshare.server.robot.ServerHeadlessTestApplication;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ServerHeadlessTestApplication.class)
@TestPropertySource(locations = {"classpath:application.properties", "classpath:application-it.properties"})
@WebAppConfiguration
public class OsServiceFactoryTest {

    private static final String OS_NAME_SYSTEM_PROPERTY = "os.name";

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Autowired
    OsServiceFactory osServiceFactory;
    @Autowired
    MsWindowsService msWindowsService;
    @Autowired
    OSxService oSxService;
    @Autowired
    LinuxService linuxService;

    @Test
    public void getOsService_returnsMsWindowsService_withWindowOs() {
        // GIVEN
        System.setProperty(OS_NAME_SYSTEM_PROPERTY, "Windows");
        // WHEN
        OsService osServiceResult = osServiceFactory.getOsService();
        // THEN
        assertThat(osServiceResult).isNotNull().isEqualTo(msWindowsService);
    }

    @Test
    public void getOsService_returnsOsXService_withMaxOsX() {
        // GIVEN
        System.setProperty(OS_NAME_SYSTEM_PROPERTY, "Mac OS X");
        // WHEN
        OsService osServiceResult = osServiceFactory.getOsService();
        // THEN
        assertThat(osServiceResult).isNotNull().isEqualTo(oSxService);
    }

    @Test
    public void getOsService_returnsLinuxService_withLinux() {
        // GIVEN
        System.setProperty(OS_NAME_SYSTEM_PROPERTY, "Linux");
        // WHEN
        OsService osServiceResult = osServiceFactory.getOsService();
        // THEN
        assertThat(osServiceResult).isNotNull().isEqualTo(linuxService);
    }


    @Test
    public void getOsService_throwsUnsupportedOsException_withUnhandledOs() {
        // GIVEN
        String osName = "unhandledOs";
        System.setProperty(OS_NAME_SYSTEM_PROPERTY, osName);
        // THEN
        thrown.expect(UnsupportedOsException.class);
        thrown.expectMessage(osName);

        // WHEN
        osServiceFactory.getOsService();
    }
}
