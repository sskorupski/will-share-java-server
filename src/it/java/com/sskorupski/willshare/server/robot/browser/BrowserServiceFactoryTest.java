package com.sskorupski.willshare.server.robot.browser;

import com.sskorupski.willshare.server.WillShareServerApplication;
import com.sskorupski.willshare.server.robot.ServerHeadlessTestApplication;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.assertj.core.api.Assertions.*;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ServerHeadlessTestApplication.class)
@TestPropertySource(value = {"classpath:application-it.properties"})
@WebAppConfiguration
public class BrowserServiceFactoryTest {

    @Autowired
    BrowserServiceFactory browserServiceFactory;
    @Autowired
    ChromeService chromeService;
    @Autowired
    DefaultBrowserService defaultBrowserService;

    @Test
    public void getBrowserService_returnsChromeService_withChromeBrowserName() {
        // GIVEN
        String browserName = "Chrome";
        // WHEN
        BrowserService browserServiceResult = browserServiceFactory.getBrowserService(browserName);
        // THEN
        assertThat(browserServiceResult)
                .isNotNull()
                .isEqualTo(chromeService);
    }

    @Test
    public void getBrowserService_returnsDefaultBrowserService_withNullBrowserName() {
        // GIVEN
        String browserName = null;
        // WHEN
        BrowserService browserServiceResult = browserServiceFactory.getBrowserService(browserName);
        // THEN
        assertThat(browserServiceResult)
                .isNotNull()
                .isEqualTo(defaultBrowserService);
    }

    @Test
    public void getBrowserService_returnsDefaultBrowserService_withUnknownBrowserName() {
        // GIVEN
        String browserName = "unknownBrowserName";
        // WHEN
        BrowserService browserServiceResult = browserServiceFactory.getBrowserService(browserName);
        // THEN
        assertThat(browserServiceResult)
                .isNotNull()
                .isEqualTo(defaultBrowserService);
    }
}
