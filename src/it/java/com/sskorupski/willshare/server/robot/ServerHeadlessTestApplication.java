package com.sskorupski.willshare.server.robot;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import javax.swing.*;

@SpringBootApplication
public class ServerHeadlessTestApplication {

    public static ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(ServerHeadlessTestApplication.class);
        applicationContext = builder.headless(true).run(args);
    }

}