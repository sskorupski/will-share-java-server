import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressTest {

    @Test
    public void hostname() throws UnknownHostException {
        String hostname = InetAddress.getLocalHost().getHostName();
        System.out.println(hostname);
    }
}
