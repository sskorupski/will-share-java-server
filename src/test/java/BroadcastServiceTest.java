import com.sskorupski.willshare.server.network.BroadcastService;
import org.junit.Test;

import java.io.IOException;


public class BroadcastServiceTest {

    @Test
    public void multicastResponse() throws IOException {
        new BroadcastService().multicast("230.0.0.0", 8042, "Hello client !");
    }

    @Test
    public void multicastGreeting() throws IOException {
        new BroadcastService().multicast("230.0.0.0", 8042, "Hello Will !");
    }
}