package com.sskorupski.willshare.server.persistence;

public class AllowedClient {

    private String client;
    private String secretKey;

    public AllowedClient() {
    }

    public String getClient() {
        return client;
    }

    public AllowedClient setClient(String client) {
        this.client = client;
        return this;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public AllowedClient setSecretKey(String secretKey) {
        this.secretKey = secretKey;
        return this;
    }
}
