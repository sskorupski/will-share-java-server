package com.sskorupski.willshare.server;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class ExampleAspect {

    @Before("execution(* com.sskorupski.willshare.server.systray.icon.WillTrayIcon.update(..))")
    public Object logExecutionTime(JoinPoint joinPoint) throws Throwable {
        return null;
    }
}