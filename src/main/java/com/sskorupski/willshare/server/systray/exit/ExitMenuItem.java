package com.sskorupski.willshare.server.systray.exit;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

public class ExitMenuItem extends MenuItem {

    public static final String EXIT_KEY = "label.exit";

    public ExitMenuItem(ResourceBundle resourceBundle) {
        super();
        setLabel(resourceBundle.getString(EXIT_KEY));
        addActionListener(new OnClickExitListener());

    }
}
