package com.sskorupski.willshare.server.systray.icon;

import com.sskorupski.willshare.server.systray.SystrayPopupMenu;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class WillTrayIconBuilder {

    private final String imagePath;
    private String tooltip;
    private SystrayPopupMenu systrayPopupMenu;

    public WillTrayIconBuilder(String imagePath) {
        this.imagePath = imagePath;
    }

    public WillTrayIconBuilder tooltip(String tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public WillTrayIcon build() throws IOException {
        WillTrayIcon trayIcon = new WillTrayIcon(createImage(imagePath), systrayPopupMenu);
        if (!StringUtils.isEmpty(tooltip)) {
            trayIcon.setToolTip(tooltip);
        }

        return trayIcon;
    }

    Image createImage(String imagePath) throws IOException {
        BufferedImage trayIconImage = ImageIO.read(getClass().getResource(imagePath));
        int trayIconWidth = new TrayIcon(trayIconImage).getSize().width;
        return trayIconImage.getScaledInstance(trayIconWidth, -1, Image.SCALE_SMOOTH);
    }

    public WillTrayIconBuilder systrayPopupMenu(SystrayPopupMenu systrayPopupMenu) {
        this.systrayPopupMenu = systrayPopupMenu;
        return this;
    }
}
