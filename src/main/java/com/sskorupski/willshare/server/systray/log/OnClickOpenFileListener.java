package com.sskorupski.willshare.server.systray.log;

import com.sskorupski.willshare.server.robot.browser.BrowserRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class OnClickOpenFileListener implements ActionListener {
    Logger LOGGER = LoggerFactory.getLogger(OnClickOpenFileListener.class);
    private final String filepath;

    public OnClickOpenFileListener(String filepath) {
        this.filepath = filepath;
        LOGGER.info("Log filepath={}", filepath);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Desktop desktop = Desktop.getDesktop();
        File file = new File(filepath);
        if (file.exists()) {
            try {
                desktop.open(file);
            } catch (IOException e1) {
                LOGGER.error("Error while opening file={}", filepath);
            }
        }
    }
}
