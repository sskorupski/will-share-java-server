package com.sskorupski.willshare.server.systray;

import com.sskorupski.willshare.server.preference.PreferenceDAO;
import com.sskorupski.willshare.server.systray.icon.WillTrayIcon;
import com.sskorupski.willshare.server.systray.icon.WillTrayIconBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

@Configuration
public class Systray {
    Logger LOGGER = LoggerFactory.getLogger(Systray.class);

    public static final String SYSTRAY_ICON_PATH = "/systray/systray-icon.png";

    @Autowired
    SystrayPopupMenu systrayPopupMenu;

    @Autowired
    PreferenceDAO preferenceDAO;

    @Value("${spring.application.name}")
    private String appName;

    private WillTrayIcon trayIcon;

    @PostConstruct
    public void initSystray() throws IOException, ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        if (!SystemTray.isSupported()) {
            LOGGER.error("SystemTray is not supported");
            return;
        }
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        trayIcon = new WillTrayIconBuilder(SYSTRAY_ICON_PATH)
                .tooltip(appName)
                .systrayPopupMenu(systrayPopupMenu)
                .build();
        trayIcon.update();

        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        final SystemTray tray = SystemTray.getSystemTray();
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("WillTrayIconBuilder could not be added.");
        }


    }

}
