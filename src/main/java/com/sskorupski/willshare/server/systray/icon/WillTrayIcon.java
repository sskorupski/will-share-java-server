package com.sskorupski.willshare.server.systray.icon;

import com.sskorupski.willshare.server.systray.SystrayPopupMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class WillTrayIcon extends TrayIcon {

    private static final Logger LOGGER = LoggerFactory.getLogger(WillTrayIcon.class);

    public WillTrayIcon(Image image, SystrayPopupMenu systrayPopupMenu) {
        super(image);
        setPopupMenu(systrayPopupMenu);
    }

    public void update(){

    }



}
