package com.sskorupski.willshare.server.systray.cast;

import com.sskorupski.willshare.server.preference.PreferenceDAO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ResourceBundle;

@org.springframework.stereotype.Component
public class AllowCastMenuItem extends CheckboxMenuItem implements ItemListener {

    public static final String ALLOW_CAST_KEY = "label.cast.allow";
    public static final String CAST_ALLOW_PREFERENCE_KEY = "cast.allow";

    @Autowired
    private PreferenceDAO preferenceDAO;
    @Autowired
    private ResourceBundle systrayResourceBundle;

    @PostConstruct
    public void init() {
        setLabel(systrayResourceBundle.getString(ALLOW_CAST_KEY));
        setState(preferenceDAO.isEnable(CAST_ALLOW_PREFERENCE_KEY));
        addItemListener(this);
    }

    @Override
    public void itemStateChanged(ItemEvent itemEvent) {
        preferenceDAO.enable(
                CAST_ALLOW_PREFERENCE_KEY,
                itemEvent.getStateChange() == ItemEvent.SELECTED
        );
    }
}
