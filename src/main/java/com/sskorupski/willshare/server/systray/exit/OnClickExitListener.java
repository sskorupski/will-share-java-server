package com.sskorupski.willshare.server.systray.exit;

import com.sskorupski.willshare.server.WillShareServerApplication;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OnClickExitListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        WillShareServerApplication.applicationContext.close();
        System.exit(0);
    }
}
