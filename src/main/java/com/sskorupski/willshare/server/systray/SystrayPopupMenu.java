package com.sskorupski.willshare.server.systray;

import com.sskorupski.willshare.server.print.PrintRestController;
import com.sskorupski.willshare.server.systray.cast.AllowCastMenuItem;
import com.sskorupski.willshare.server.systray.exit.ExitMenuItem;
import com.sskorupski.willshare.server.systray.log.LogMenuItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

@Component
public class SystrayPopupMenu extends PopupMenu {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystrayPopupMenu.class);

    @Value("${logging.file}")
    private String loggingFile;
    @Autowired
    ResourceBundle systrayResourceBundle;

    @Autowired
    AllowCastMenuItem allowCastMenuItem;

    public SystrayPopupMenu() {
    }


    @PostConstruct
    public void init() {
        add(new LogMenuItem(systrayResourceBundle, loggingFile));
        add(allowCastMenuItem);
        addSeparator();
        add(new ExitMenuItem(systrayResourceBundle));

        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("{}", e);
            }
        });
    }
}
