package com.sskorupski.willshare.server.systray.log;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

public class LogMenuItem extends MenuItem {

    public static final String OPEN_LOG_KEY = "label.log.open";

    public LogMenuItem(ResourceBundle resourceBundle, String logFilepath) {
        super();
        setLabel(resourceBundle.getString(OPEN_LOG_KEY));
        addActionListener(new OnClickOpenFileListener(logFilepath));
    }
}
