package com.sskorupski.willshare.server.network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

@Service
public class BroadcastService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BroadcastService.class);

    public BroadcastService() {
    }

    /**
     * Iterate through all NetworkInterfaces to find their broadcast address
     */
    public List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastListResult = new ArrayList<>();

        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();

            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }

            networkInterface.getInterfaceAddresses().stream()
                    .map(a -> a.getBroadcast())
                    .filter(Objects::nonNull)
                    .forEach(broadcastListResult::add);
        }

        return broadcastListResult;
    }


    public void multicast(String inetAddress, Integer port, String multicastMessage) throws IOException {
        DatagramSocket socket = new DatagramSocket();
        InetAddress group = InetAddress.getByName(inetAddress);
        byte[] buf = multicastMessage.getBytes();

        DatagramPacket packet = new DatagramPacket(buf, buf.length, group, port);
        socket.send(packet);
        socket.close();

        LOGGER.info(String.format("address={%s:%d} sentMessage={%s}", inetAddress, port, multicastMessage));
    }

}