package com.sskorupski.willshare.server.network;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.List;

@RequestMapping("/broadcast")
@RestController
public class BroadcastRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BroadcastRestController.class);

    @Autowired
    BroadcastService broadcastService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<InetAddress> listAll() throws SocketException {
        return broadcastService.listAllBroadcastAddresses();
    }

    @RequestMapping(value = "/listen/{inetAddress}/{port}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> listen(@PathVariable("inetAddress") String inetAddress, @PathVariable("port") Integer port) throws IOException {
        LOGGER.info(String.format("listen={%s:%d}", inetAddress, port));

        ensureIsValidInetAddress(inetAddress);

        try {
            MulticastReceiver multicastReceiver = new MulticastReceiver(inetAddress, port);
            multicastReceiver.start();
        } catch (Exception exception) {
            LOGGER.error(String.format("Failed to listen={%s:%d}", inetAddress, port), exception);
            return new ResponseEntity(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void ensureIsValidInetAddress(@PathVariable("inetAddress") String inetAddress) {
        if (!InetAddressValidator.getInstance().isValid(inetAddress)) {
            throw new IllegalArgumentException(String.format("inetAddress={%s} is not a valid inetAddress", inetAddress));
        }
    }

    @RequestMapping(value = "/send/{inetAddress}/{port}", method = RequestMethod.PUT)
    public void sendMulticast(
            @PathVariable("inetAddress") String inetAddress,
            @PathVariable("port") Integer port,
            @RequestBody String multicastMessage) throws IOException {

        LOGGER.info(String.format("sendMulticast={%s:%d} multicastMessage={%s}", inetAddress, port, multicastMessage));

        ensureIsValidInetAddress(inetAddress);
        broadcastService.multicast(inetAddress, port, multicastMessage);
    }


}