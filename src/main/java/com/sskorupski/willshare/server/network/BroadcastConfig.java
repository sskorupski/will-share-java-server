package com.sskorupski.willshare.server.network;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Configuration
public class BroadcastConfig {

    @Autowired
    BroadcastRestController broadcastRestController;

    @PostConstruct
    public void initListening() throws IOException {
        broadcastRestController.listen("230.0.0.0", 8042);
    }
}
