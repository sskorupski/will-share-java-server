package com.sskorupski.willshare.server.network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MulticastReceiver extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(MulticastReceiver.class);
    public static final String STOP_LISTENING = "STOP LISTENING";
    private static final String GREETING = "Hello Will !";
    private static final String GREETING_ANSWER = "Hello client !";

    protected final InetAddress inetGroup;
    protected MulticastSocket socket = null;

    protected byte[] buf = new byte[256];

    public MulticastReceiver(String listenAddress, int port) throws IOException {
        super(String.format("MulticastReceiver={%s:%d}", listenAddress, port));
        socket = new MulticastSocket(port);
        inetGroup = InetAddress.getByName(listenAddress);
        socket.joinGroup(inetGroup);

        LOGGER.info(String.format("Multicast socket prepared on address={%s:%d}", listenAddress, port));
    }


    @Override
    public void run() {
        try {
            int listeningPort = socket.getLocalPort();
            LOGGER.info(String.format("Multicast listening address={%s:%d}",
                    inetGroup.toString(),
                    listeningPort));

            while (true) {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);

                String receivedMessage = new String(packet.getData(), 0, packet.getLength());
                LOGGER.info(String.format("from={%s} receivedMessage={%s}", packet.getAddress().toString(), receivedMessage));
                if (STOP_LISTENING.equals(receivedMessage)) {
                    break;
                }
                if (GREETING.equalsIgnoreCase(receivedMessage)) {
                    new BroadcastService().multicast(inetGroup.getHostAddress(), listeningPort, GREETING_ANSWER);
                }
            }

            socket.leaveGroup(inetGroup);
            socket.close();

            LOGGER.info(String.format("Multicast listening stopped address={%s:%d}",
                    inetGroup.toString(),
                    listeningPort));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}