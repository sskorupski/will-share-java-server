package com.sskorupski.willshare.server.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@Component
public class LoggerRequestInterceptor extends HandlerInterceptorAdapter implements WebMvcConfigurer {

    public static final Logger LOGGER = LoggerFactory.getLogger(LoggerRequestInterceptor.class);
    public static final String LOG_FROM_METHOD_URI = "from={{}} method={{}} URI={{}}";

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this);
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        LOGGER.trace(LOG_FROM_METHOD_URI,
                request.getRemoteAddr(),
                request.getMethod(),
                request.getRequestURI()
        );
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) {
        // No-ops
    }


}