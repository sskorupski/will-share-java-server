package com.sskorupski.willshare.server.locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;
import java.util.ResourceBundle;

@Configuration
public class ResourceBundleConfig {

    public ResourceBundle resourceBundle(String baseName) {
        Locale currentLocale = Locale.getDefault();
        return ResourceBundle.getBundle(baseName, currentLocale);
    }

    @Bean
    public ResourceBundle systrayResourceBundle() {
        return resourceBundle("systray");
    }
}
