package com.sskorupski.willshare.server.preference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class PreferenceDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public int enable(String key, boolean enabled) {
        return this.jdbcTemplate.update(
                "UPDATE preferences SET value=? WHERE key=? ",
                enabled ? 1 : 0,
                key
        );
    }

    public boolean isEnable(String key) {
        int enabled = this.jdbcTemplate.queryForObject(
                "SELECT value FROM preferences WHERE key=? ",
                Integer.class,
                key
        );
        return enabled == 0 ? false : true;
    }
}