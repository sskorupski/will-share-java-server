package com.sskorupski.willshare.server.preference;

public class Preference<VALUE> {
    public static String PREFERENCES_TABLE_NAME = "preferences";

    private String key;
    private VALUE value;

    public Preference() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public VALUE getValue() {
        return value;
    }

    public void setValue(VALUE value) {
        this.value = value;
    }
}
