package com.sskorupski.willshare.server.robot.browser;

import java.io.IOException;
import java.net.URISyntaxException;

public interface BrowserService {

    String getBrowserName();

    void browserHistory();
    void closeTab();
    void favorite();
    void launchBrowser(String url) throws URISyntaxException, IOException;
    void nextPage();
    void nextTab();
    void previousPage();
    void restoreTab();

    void search();
}
