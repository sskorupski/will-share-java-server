package com.sskorupski.willshare.server.robot.os;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("/robot/os")
@RestController
public class OsRestController {

    Logger LOGGER = LoggerFactory.getLogger(OsRestController.class);

    OsService windowService;

    public OsRestController(@Autowired OsServiceFactory osServiceFactory) {
        windowService = osServiceFactory.getOsService();
    }

    @RequestMapping(value = "/close-window", method = RequestMethod.DELETE)
    public void closeWindow(){
        windowService.closeWindow();
    }

    @RequestMapping(value = "/move-window/{direction}", method = RequestMethod.PUT)
    public void closeWindow(@PathVariable(value = "direction") int direction)  {
        windowService.moveWindow(direction);
    }

    @RequestMapping(value = "/minimize-window", method = RequestMethod.PUT)
    public void minimizeWindow()  {
        windowService.minimizeWindow();
    }

    @RequestMapping(value = "/maximize-window", method = RequestMethod.PUT)
    public void maximizeWindow()  {
        windowService.maximizeWindow();
    }

    @RequestMapping(value = "/minmax-all-windows", method = RequestMethod.PUT)
    public void minmaxAllWindows()  {
        windowService.minmaxWindow();
    }

    @RequestMapping(value = "/new-window", method = RequestMethod.POST)
    public void newWindow()  {
        windowService.newWindow();
    }

    @RequestMapping(value = "/refresh-window", method = RequestMethod.PUT)
    public void refreshWindow()  {
        windowService.refreshWindow();
    }

    @RequestMapping(value = "/task-manager", method = RequestMethod.POST)
    public void openTaskManager()  {
        windowService.openTaskManager();
    }
}
