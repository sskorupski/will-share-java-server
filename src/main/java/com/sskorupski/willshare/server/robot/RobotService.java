package com.sskorupski.willshare.server.robot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Service
public class RobotService {

    public static final int DEFAULT_DELAY = 0;
    public static final int CURSOR_SIZE = 24;
    private final Image cursor;
    Logger LOGGER = LoggerFactory.getLogger(RobotService.class);

    private Robot robot = new Robot();
    private Integer delayInMs = DEFAULT_DELAY;

    public RobotService() throws AWTException, IOException {
        cursor = ImageIO.read(getClass().getResourceAsStream("/robot/cursor.png"));
    }

    public void type(Character letter) {
        robot.delay(DEFAULT_DELAY);
        int keyChar = KeyStroke.getKeyStroke(letter, 0).getKeyChar();

        robot.keyPress(KeyEvent.VK_ALT);

        // Permet la prise en compte des caractères spéciaux
        for (int i = 3; i >= 0; --i) {
            // extracts a single decade of the key-code and adds an offset to get the required VK_NUMPAD key-code
            int nampadKeyChar = keyChar / (int) (Math.pow(10, i)) % 10 + KeyEvent.VK_NUMPAD0;

            robot.keyPress(nampadKeyChar);
            robot.keyRelease(nampadKeyChar);
        }

        robot.keyRelease(KeyEvent.VK_ALT);
    }

    public void setDelayInMs(Integer delayInMs) {
        this.delayInMs = delayInMs;
    }

    /**
     * @param buttonMask {@link InputEvent}
     * @param x          la position horizontale où 0 est le bord gauche et 1 le bord droit de l'écran
     * @param y          la position verticale où 0 est le bord supérieur et 1 le bord inférieur de l'écran
     */
    public void click(int buttonMask, Double x, Double y) {
        robot.delay(DEFAULT_DELAY);
        moveToByRelativePosition(x, y);

        robot.mousePress(buttonMask);
        robot.mouseRelease(buttonMask);
    }

    private void moveToByRelativePosition(Double x, Double y) {
        Point offsetPoint = ratioToPixel(x, y);
        Point currentMousePosition = MouseInfo.getPointerInfo().getLocation();

        robot.mouseMove(
                (int) (offsetPoint.getX() + currentMousePosition.getX()),
                (int) (offsetPoint.getY() + currentMousePosition.getY())
        );
    }

    protected Point ratioToPixel(double x, double y) {
        Point pointResult = new Point();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double screenWidth = screenSize.getWidth();
        double screenHeight = screenSize.getHeight();

        pointResult.setLocation((int) (x * screenWidth), (int) (y * screenHeight));
        return pointResult;
    }

    public void doubleClick(Double x, Double y) {
        robot.delay(DEFAULT_DELAY);

        moveToByRelativePosition(x, y);

        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);

        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    public void moveRelative(Double x, Double y) {
        robot.delay(DEFAULT_DELAY);
        moveToByRelativePosition(x, y);
    }

    public void click(Integer buttonMask) {
        robot.delay(DEFAULT_DELAY);

        robot.mousePress(buttonMask);
        robot.mouseRelease(buttonMask);
    }

    public void type(int keyCode) {
        robot.keyPress(keyCode);
    }

    public void keyPressAndRelease(int... keys) {
        for (int i = 0; i < keys.length; i++) {
            robot.keyPress(keys[i]);
        }
        for (int i = 0; i < keys.length; i++) {
            robot.keyRelease(keys[i]);
        }
    }

    public BufferedImage screenshot() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRectangle = new Rectangle(screenSize);
        return robot.createScreenCapture(screenRectangle);
    }

    public BufferedImage screenshot(boolean captureMousePosition) {
        if (!captureMousePosition) {
            return screenshot();
        } else {
            BufferedImage bufferedScreenshot = screenshot();
            Point mousePosition = MouseInfo.getPointerInfo().getLocation();
            Graphics2D graphics2D = bufferedScreenshot.createGraphics();
            graphics2D.drawImage(cursor,
                    mousePosition.x,
                    mousePosition.y,
                    CURSOR_SIZE,
                    CURSOR_SIZE,
                    null);
            return bufferedScreenshot;
        }
    }

    public void multiPressAndRelase(int... keys) {
        for (int key : keys) {
            robot.keyPress(key);
        }
        for (int key : keys) {
            robot.keyRelease(key);
        }
    }

}
