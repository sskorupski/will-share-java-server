package com.sskorupski.willshare.server.robot.os;

public class UnsupportedOsException extends RuntimeException {

    public UnsupportedOsException(String operatingSystem) {
        super(operatingSystem);
    }

}
