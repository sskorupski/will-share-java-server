package com.sskorupski.willshare.server.robot;

import com.sskorupski.willshare.server.preference.PreferenceDAO;
import com.sskorupski.willshare.server.systray.cast.AllowCastMenuItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;

@RequestMapping("/robot")
@RestController
public class RobotRestController {

    Logger LOGGER = LoggerFactory.getLogger(RobotRestController.class);

    @Autowired
    RobotService robotService;
    @Autowired
    PreferenceDAO preferenceDAO;


    @RequestMapping(value = "/delay/{delayInMs}", method = RequestMethod.PUT)
    public void setDelayInMs(@PathVariable(value = "delayInMs") Integer delayInMs) {
        robotService.setDelayInMs(delayInMs);
    }

    @RequestMapping(value = "/type-letter/{letter}", method = RequestMethod.PUT)
    public void typeLetter(@PathVariable(value = "letter") Character letter) {
        robotService.type(letter);
    }

    @RequestMapping(value = "/type-keycode/{keyCode}", method = RequestMethod.PUT)
    public void typeKeyCode(@PathVariable(value = "keyCode") int keyCode) {
        robotService.type(keyCode);

    }

    /**
     * @see RobotService#click(int, Double, Double)
     */
    @RequestMapping(value = "/click/{button}", method = RequestMethod.PUT)
    public void click(@PathVariable(value = "button") String button) {
        Optional<Integer> inputEvent = MouseButtonInputEventEnum.inputEventFromName(button);
        if (!inputEvent.isPresent()) {
            throw new IllegalArgumentException(
                    String.format("Le bouton {%s} n'est pas pris en charge", button)
            );
        }
        robotService.click(inputEvent.get());
    }

    /**
     * @see RobotService#click(int, Double, Double)
     */
    @RequestMapping(value = "/click/{button}/{x}/{y}", method = RequestMethod.PUT)
    public void clickPosition(
            @PathVariable(value = "button") String button,
            @PathVariable(value = "x") Double x,
            @PathVariable(value = "y") Double y) {
        Optional<Integer> inputEvent = MouseButtonInputEventEnum.inputEventFromName(button);
        if (!inputEvent.isPresent()) {
            throw new IllegalArgumentException(String.format("Le bouton {%s} n'est pas pris en charge", button));
        }
        robotService.click(inputEvent.get(), x, y);
    }

    /**
     * @see RobotService#click(int, Double, Double)
     */
    @RequestMapping(value = "/double-click/{x}/{y}", method = RequestMethod.PUT)
    public void doubleClick(@PathVariable(value = "x") Double x, @PathVariable(value = "y") Double y) {
        robotService.doubleClick(x, y);
    }

    @RequestMapping(value = "/move-relative/{x}/{y}", method = RequestMethod.PUT)
    public void mouseMove(@PathVariable(value = "x") Double x, @PathVariable(value = "y") Double y) {
        robotService.moveRelative(x, y);
    }

    @RequestMapping(value = "/screenshot", method = RequestMethod.GET)
    public void screenshot(HttpServletResponse response) throws IOException {
        if (preferenceDAO.isEnable(AllowCastMenuItem.CAST_ALLOW_PREFERENCE_KEY)) {
            BufferedImage bufferedImage = robotService.screenshot(true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            ImageIO.write(bufferedImage, "jpg", response.getOutputStream());
        } else {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

}
