package com.sskorupski.willshare.server.robot.os;

import com.sskorupski.willshare.server.robot.RobotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.event.KeyEvent;
import java.io.IOException;

@Service
public class MsWindowsService implements OsService {

    Logger LOGGER = LoggerFactory.getLogger(MsWindowsService.class);

    @Autowired
    RobotService robotService;

    @Override
    public String getOsName() {
        return "Windows";
    }

    public void closeWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_ALT, KeyEvent.VK_F4);
    }

    public void moveWindow(int direction) {
        robotService.multiPressAndRelase(KeyEvent.VK_WINDOWS, direction);
    }

    public void minimizeWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_WINDOWS, KeyEvent.VK_DOWN);
    }

    public void maximizeWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_WINDOWS, KeyEvent.VK_UP);
    }

    public void minmaxWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_WINDOWS, KeyEvent.VK_D);
    }

    public void newWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_N);
    }

    @Override
    public void shutdown() throws IOException {
        Runtime runtime = Runtime.getRuntime();
        Process proc = runtime.exec("shutdown -s -t 0");
        System.exit(0);
    }

    public void refreshWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_F5);
    }

    public void openTaskManager() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_SHIFT, KeyEvent.VK_ESCAPE);
    }

}
