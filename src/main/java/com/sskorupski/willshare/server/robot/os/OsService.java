package com.sskorupski.willshare.server.robot.os;

import java.io.IOException;

public interface OsService {

    String getOsName();
    void closeWindow();
    void maximizeWindow();
    void minimizeWindow();
    void minmaxWindow();
    void moveWindow(int direction);
    void openTaskManager();
    void newWindow();
    void shutdown() throws IOException;
    void refreshWindow();
}
