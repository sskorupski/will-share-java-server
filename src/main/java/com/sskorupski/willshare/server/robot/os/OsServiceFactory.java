package com.sskorupski.willshare.server.robot.os;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class OsServiceFactory {

    Logger LOGGER = LoggerFactory.getLogger(OsServiceFactory.class);

    private Collection<OsService> windowServices;


    public OsServiceFactory(@Autowired ApplicationContext applicationContext) {
        windowServices = applicationContext.getBeansOfType(OsService.class).values();
    }

    public OsService getOsService() {
        String operatingSystem = System.getProperty("os.name");
        LOGGER.info("os.name={{}}", operatingSystem);
        for (OsService aWindowService : windowServices) {
            if (operatingSystem.contains(aWindowService.getOsName())) {
                return aWindowService;
            }
        }
        throw new UnsupportedOsException(operatingSystem);
    }

}
