package com.sskorupski.willshare.server.robot.browser;

import com.sskorupski.willshare.server.robot.RobotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.event.KeyEvent;

@Service
public class ChromeService extends DefaultBrowserService{

    public static final String CHROME_BROWSER_NAME = "Chrome";
    @Autowired
    RobotService robotService;

    @Override
    public String getBrowserName() {
        return CHROME_BROWSER_NAME;
    }

    Logger LOGGER = LoggerFactory.getLogger(ChromeService.class);

    @Override
    public void favorite()  {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_SHIFT, KeyEvent.VK_O);
    }

}
