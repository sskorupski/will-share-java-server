package com.sskorupski.willshare.server.robot.browser;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class BrowserServiceFactory {

    Logger LOGGER = LoggerFactory.getLogger(BrowserServiceFactory.class);

    @Autowired
    private DefaultBrowserService defaultBrowserService;

    private Collection<BrowserService> browserServices;

    public BrowserServiceFactory(@Autowired ApplicationContext applicationContext) {
        browserServices = applicationContext.getBeansOfType(BrowserService.class).values();
    }

    public BrowserService getBrowserService(String browserName) {
        if (browserName == null || browserName.trim().length() == 0) {
            return defaultBrowserService;
        }
        for (BrowserService aBrowserService : browserServices) {
            if (StringUtils.contains(browserName, aBrowserService.getBrowserName())) {
                return aBrowserService;
            }
        }
        return defaultBrowserService;
    }
}
