package com.sskorupski.willshare.server.robot.browser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;

@RequestMapping("/robot/browser")
@RestController
public class BrowserRestController {

    public static final String USER_AGENT = "User-Agent";
    Logger LOGGER = LoggerFactory.getLogger(BrowserRestController.class);

    @Autowired
    BrowserServiceFactory browserServiceFactory;

    private BrowserService browserService(@RequestHeader(value = "User-Agent") String userAgent) {
        return browserServiceFactory.getBrowserService(userAgent);
    }

    @RequestMapping(value = "/browser", method = RequestMethod.POST)
    public void launchBrowser(@RequestHeader(value = USER_AGENT) String userAgent,
                              @RequestBody(required = false) String url)
            throws URISyntaxException, IOException {
        browserService(userAgent).launchBrowser(url);
    }


    @RequestMapping(value = "/history", method = RequestMethod.POST)
    public void browserHistory(@RequestHeader(value = USER_AGENT) String userAgent) {
        browserService(userAgent).browserHistory();
    }

    @RequestMapping(value = "/previous-page", method = RequestMethod.PUT)
    public void previousPage(@RequestHeader(value = USER_AGENT) String userAgent) {
        browserService(userAgent).previousPage();
    }

    @RequestMapping(value = "/next-page", method = RequestMethod.PUT)
    public void nextPage(@RequestHeader(value = USER_AGENT) String userAgent) {
        browserService(userAgent).nextPage();
    }

    @RequestMapping(value = "/new-tab", method = RequestMethod.POST)
    public void nextTab(@RequestHeader(value = USER_AGENT) String userAgent) {
        browserService(userAgent).nextTab();
    }

    @RequestMapping(value = "/restore-tab", method = RequestMethod.POST)
    public void restoreTab(@RequestHeader(value = USER_AGENT) String userAgent) {
        browserService(userAgent).restoreTab();
    }

    @RequestMapping(value = "/close-tab", method = RequestMethod.DELETE)
    public void closeTab(@RequestHeader(value = USER_AGENT) String userAgent) {
        browserService(userAgent).closeTab();
    }

    @RequestMapping(value = "/search", method = RequestMethod.PUT)
    public void search(@RequestHeader(value = USER_AGENT) String userAgent) {
        browserService(userAgent).search();
    }

}
