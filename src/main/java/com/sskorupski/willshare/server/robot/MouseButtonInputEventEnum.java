package com.sskorupski.willshare.server.robot;

import java.awt.event.InputEvent;
import java.util.Optional;

public enum MouseButtonInputEventEnum {
    LEFT("left", InputEvent.BUTTON1_MASK),
    MIDDLE("middle", InputEvent.BUTTON3_MASK),
    RIGHT("right", InputEvent.BUTTON2_MASK);

    private final String name;
    private final int inputEventMask;

    MouseButtonInputEventEnum(String name, int inputEventMask) {
        this.name = name;
        this.inputEventMask = inputEventMask;
    }

    /**
     * @param nom {@link #name}
     * @return le masque {@link InputEvent} associé au <code>nom</code> du click
     */
    public static Optional<Integer> inputEventFromName(String nom) {
        for (MouseButtonInputEventEnum inputEventEnum : values()) {
            if (inputEventEnum.name.equals(nom)) {
                return Optional.of(inputEventEnum.inputEventMask);
            }
        }
        return Optional.empty();
    }
}
