package com.sskorupski.willshare.server.robot.os;

import com.sskorupski.willshare.server.robot.RobotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.event.KeyEvent;
import java.io.IOException;

@Service
public class LinuxService implements OsService {

    Logger LOGGER = LoggerFactory.getLogger(OsService.class);

    @Autowired
    RobotService robotService;

    @Override
    public String getOsName() {
        return "Linux";
    }

    @Override
    public void closeWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_ALT, KeyEvent.VK_F4);
    }

    @Override
    public void maximizeWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_ALT, KeyEvent.VK_SPACE);
        robotService.keyPressAndRelease(KeyEvent.VK_X);
    }

    @Override
    public void minimizeWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_ALT, KeyEvent.VK_SPACE);
        robotService.keyPressAndRelease(KeyEvent.VK_N);
    }

    @Override
    public void minmaxWindow() {
        LOGGER.error("Minmax os feature have to be configured");
    }

    @Override
    public void moveWindow(int direction) {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_WINDOWS, direction);
    }

    @Override
    public void openTaskManager() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_ESCAPE);
    }

    @Override
    public void newWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_N);
    }

    @Override
    public void shutdown() throws IOException {
        Runtime runtime = Runtime.getRuntime();
        Process proc = runtime.exec("sudo shutdown -h now");
        System.exit(0);
    }

    @Override
    public void refreshWindow() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_F5);
    }
}
