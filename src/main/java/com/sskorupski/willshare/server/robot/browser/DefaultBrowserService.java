package com.sskorupski.willshare.server.robot.browser;

import com.sskorupski.willshare.server.robot.RobotRestController;
import com.sskorupski.willshare.server.robot.RobotService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@Service
public class DefaultBrowserService implements BrowserService {

    Logger LOGGER = LoggerFactory.getLogger(RobotRestController.class);

    @Autowired
    RobotService robotService;

    @Override
    public String getBrowserName() {
        return null;
    }

    @Override
    public void launchBrowser(String url) throws URISyntaxException, IOException {
        if (StringUtils.isEmpty(url)) {
            Desktop.getDesktop().browse(new URI("http://www.google.com"));
        } else {
            Desktop.getDesktop().browse(new URI(url));
        }
    }

    @Override
    public void browserHistory() {
        robotService.keyPressAndRelease(KeyEvent.VK_CONTROL, KeyEvent.VK_H);
    }

    @Override
    public void previousPage() {
        robotService.multiPressAndRelase(KeyEvent.VK_ALT, KeyEvent.VK_LEFT);
    }

    @Override
    public void nextPage() {
        robotService.multiPressAndRelase(KeyEvent.VK_ALT, KeyEvent.VK_RIGHT);
    }

    @Override
    public void nextTab() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_T);
    }

    @Override
    public void restoreTab() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_SHIFT, KeyEvent.VK_T);
    }

    @Override
    public void closeTab() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_F4);
    }

    @Override
    public void favorite() {
        LOGGER.error("Favorite not available for browser {}", getBrowserName());
    }

    @Override
    public void search() {
        robotService.multiPressAndRelase(KeyEvent.VK_CONTROL, KeyEvent.VK_F);
    }

}
