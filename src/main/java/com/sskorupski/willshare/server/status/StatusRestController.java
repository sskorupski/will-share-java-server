package com.sskorupski.willshare.server.status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
public class StatusRestController {
    Logger LOGGER = LoggerFactory.getLogger(StatusRestController.class);

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    @ResponseBody
    public String status() {
        return "ok";
    }

    @RequestMapping(value = "/hostname", method = RequestMethod.GET)
    @ResponseBody
    public String hostname() throws UnknownHostException {

        String hostname = InetAddress.getLocalHost().getHostName();
        LOGGER.info(String.format("hostname={%s}", hostname));
        return hostname;
    }
}