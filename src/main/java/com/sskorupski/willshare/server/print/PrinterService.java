package com.sskorupski.willshare.server.print;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import java.io.*;

@Service
public class PrinterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrinterService.class);
    public static final int SINGLE_COPY = 1;

    // A utility method to return a DocFlavor object matching the
    // extension of the filename.
    public static DocFlavor getFlavorFromFilename(String filename) {
        String extension = filename.substring(filename.lastIndexOf('.') + 1);
        extension = extension.toLowerCase();
        if (extension.equals("gif"))
            return DocFlavor.INPUT_STREAM.GIF;
        else if (extension.equals("jpeg"))
            return DocFlavor.INPUT_STREAM.JPEG;
        else if (extension.equals("jpg"))
            return DocFlavor.INPUT_STREAM.JPEG;
        else if (extension.equals("png"))
            return DocFlavor.INPUT_STREAM.PNG;
        else if (extension.equals("ps"))
            return DocFlavor.INPUT_STREAM.POSTSCRIPT;
        else if (extension.equals("txt") || extension.equals("php"))
            return DocFlavor.INPUT_STREAM.AUTOSENSE;
            // Fallback: try to determine flavor from file content
        else
            return DocFlavor.INPUT_STREAM.AUTOSENSE;
    }

    public void printTextFile(InputStream fileInputStream, String filename) throws IOException, PrintException {
        PrintRequestAttributeSet attributes = new HashPrintRequestAttributeSet();
        attributes.add(new Copies(1));

        // Figure out what type of file we're printing
        DocFlavor flavor = getFlavorFromFilename(filename);
        // Create a Doc object to print from the file and flavor.
        Doc doc = new SimpleDoc(fileInputStream, flavor, null);
        // Create a print job from the service
        DocPrintJob job = PrintServiceLookup.lookupDefaultPrintService().createPrintJob();

        // Monitor the print job with a listener
        job.addPrintJobListener(new SystemOutPrintJobAdapter());

        // Now print the document, catching errors
        try {
            job.print(doc, attributes);

            // send FF to eject the page
            InputStream ff = new ByteArrayInputStream("\f".getBytes());
            Doc docff = new SimpleDoc(ff, flavor, null);
            DocPrintJob jobff = PrintServiceLookup.lookupDefaultPrintService().createPrintJob();
            jobff.print(docff, null);
        } catch (PrintException e) {
            LOGGER.error(String.format("Printing error for filename={%s}", filename), e);
            throw e;
        }
    }
}
