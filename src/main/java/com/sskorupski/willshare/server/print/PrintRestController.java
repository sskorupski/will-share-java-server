package com.sskorupski.willshare.server.print;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.print.PrintException;
import javax.print.PrintServiceLookup;
import java.io.IOException;
import java.net.SocketException;

@RequestMapping("/print")
@RestController
public class PrintRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrintRestController.class);

    @Autowired
    PrinterService printerService;

    @RequestMapping(value = "/default-print-service-name", method = RequestMethod.GET)
    @ResponseBody
    public String getDefaultPrintServiceName() throws SocketException {
        String defaultPrintServiceName = PrintServiceLookup.lookupDefaultPrintService().getName();
        LOGGER.info(String.format("defaultPrintServiceName={%s}", defaultPrintServiceName));
        return defaultPrintServiceName;
    }

    @RequestMapping(value = "/single-document", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> printSingleDocument(@RequestParam("file") MultipartFile fileToPrint) throws IOException, PrintException {
        LOGGER.info(String.format("printSingleDocument fileName={%s}", fileToPrint.getOriginalFilename()));

        String extension = FilenameUtils.getExtension(fileToPrint.getOriginalFilename());
        if (!"pdf".equalsIgnoreCase(extension)) {
            String errorMessage = String.format("Seule l'extension pdf est prise en charge");
            LOGGER.error(errorMessage);
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }

        printerService.printTextFile(fileToPrint.getInputStream(), fileToPrint.getOriginalFilename());
        return new ResponseEntity<>(HttpStatus.OK);
    }


}