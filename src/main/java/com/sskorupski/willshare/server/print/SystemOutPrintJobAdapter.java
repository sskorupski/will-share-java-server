package com.sskorupski.willshare.server.print;

import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

class SystemOutPrintJobAdapter extends PrintJobAdapter {

    public void printJobCompleted(PrintJobEvent e) {
        System.out.println("Print job complete");
    }

    public void printDataTransferCompleted(PrintJobEvent e) {
        System.out.println("Document transfered to printer");
    }

    public void printJobRequiresAttention(PrintJobEvent e) {
        System.out.println("Print job requires attention");
        System.out.println("Check printer: out of paper?");
    }

    public void printJobFailed(PrintJobEvent e) {
        System.out.println("Print job failed");
    }
}


