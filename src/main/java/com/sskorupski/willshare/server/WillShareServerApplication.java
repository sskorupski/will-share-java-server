package com.sskorupski.willshare.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import javax.swing.*;

@SpringBootApplication
public class WillShareServerApplication {

    private static Logger LOGGER = LoggerFactory.getLogger(WillShareServerApplication.class);
    public static ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        System.setProperty("file.encoding","UTF-8");
        SpringApplicationBuilder builder = new SpringApplicationBuilder(WillShareServerApplication.class);
        applicationContext = builder.headless(false).run(args);
    }

}